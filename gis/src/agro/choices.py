# coding: utf-8
from django.utils.translation import ugettext_lazy as _

PROPERTY_TYPE_PARTNER = 'PARTNER'
PROPERTY_TYPE_PRIVATE = 'PRIVATE'
PROPERTY_TYPE = ((PROPERTY_TYPE_PARTNER, _('Partner')),
                 (PROPERTY_TYPE_PRIVATE, _('Private')))
