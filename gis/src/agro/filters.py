from rest_framework_gis.filterset import GeoFilterSet
from rest_framework_gis.filters import GeometryFilter
from rest_framework_filters import filters
from .models import Parcel


class ParcelFilter(GeoFilterSet):
    intersects_geom = GeometryFilter(name='geom', lookup_expr='intersects')

    crop_name = filters.CharFilter(name='crop_name', method='crop_filter')

    def crop_filter(self, qs, name, value):
        return qs.filter(crop__name__exact=value)

    class Meta:
        model = Parcel

        fields = {
            'id': ['exact'],
            'farm_id': ['exact'],
            'crop_name': ['exact'],
        }