# coding: utf-8
from rest_framework import serializers
from .models import (Parcel,
                     Farm)


class ParcelSerializer(serializers.ModelSerializer):
    soil_name = serializers.SerializerMethodField()
    crop_name = serializers.SerializerMethodField()
    farm_name = serializers.SerializerMethodField()

    def get_soil_name(self, obj):

        return obj.soil_type.name

    def get_crop_name(self, obj):
        return obj.crop.name

    def get_farm_name(self, obj):
        return obj.farm.name

    class Meta:
        model = Parcel
        fields = ('id', 'name', 'crop_name', 'farm_name', 'soil_name', 'key_map', 'geom')


class FarmSerializer(serializers.ModelSerializer):
    crop_name = serializers.SerializerMethodField()

    def get_crop_name(self, obj):
        return obj.crop.name

    class Meta:
        model = Farm
        fields = ('id', 'name', 'adm_unit', 'crop_id', 'property_type', 'key_map', 'crop_name', 'geom')


class MeasureUnit(object):

    def __init__(self, unit, symbol, measure_type):
        self.unit = unit
        self.symbol = symbol
        self.measure_type = measure_type


class MeasureUnitSerializer(serializers.Serializer):

    unit = serializers.CharField()
    symbol = serializers.CharField()
    measure_type = serializers.CharField()
