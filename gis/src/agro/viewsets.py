from rest_framework import viewsets
from .models import (Parcel,
                     Farm)
from .serializers import (ParcelSerializer,
                          FarmSerializer,)
from .filters import ParcelFilter


class ParcelViewSet(viewsets.ModelViewSet):

    queryset = Parcel.objects.all()
    serializer_class = ParcelSerializer
    filter_class = ParcelFilter


class FarmViewSet(viewsets.ModelViewSet):

    queryset = Farm.objects.all()
    serializer_class = FarmSerializer
