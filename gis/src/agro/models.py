# coding: utf-8
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from common.models import (
    DateRangeMixIn,
    CreatedByMixIn,
    DateCreatedMixIn,
    DateUpdatedMixIn,
)
from .choices import (PROPERTY_TYPE,
                      PROPERTY_TYPE_PARTNER,)


class SoilType(models.Model):
    name = models.CharField(max_length=20,
                            verbose_name=_('Soil Name'))


class Crop(DateRangeMixIn,
           CreatedByMixIn,
           DateCreatedMixIn,
           DateUpdatedMixIn):
    name = models.CharField(max_length=50, verbose_name=_('Crop Name'))


class Farm(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('Farm name'))
    adm_unit = models.CharField(max_length=3,
                                verbose_name=_('Administrative Unit'))
    property_type = models.CharField(max_length=7,
                                     verbose_name=_('Type'),
                                     choices=PROPERTY_TYPE,
                                     default=PROPERTY_TYPE_PARTNER)

    crop = models.ForeignKey(Crop, verbose_name=_('Crop'))

    geom = models.MultiPolygonField(srid=settings.DEFAULT_SRID)

    def get_key_map(self):
        return "{0:s}{1:s}{id:06d}".format(
            self.crop.name,
            self.adm_unit,
            id=self.id
        )
    key_map = property(get_key_map)


class Parcel(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('Parcel Name'))

    crop = models.ForeignKey(Crop, verbose_name=_('Crop'))
    farm = models.ForeignKey(Farm, verbose_name=_('Farm'))
    soil_type = models.ForeignKey(SoilType, verbose_name=_('Soil Type'))

    geom = models.PolygonField(srid=settings.DEFAULT_SRID)

    def get_key_map(self):
        farm_key_map = self.farm.get_key_map()
        return "{0:s}{parcel_id:06d}".format(
            farm_key_map,
            parcel_id=self.id
        )

    key_map = property(get_key_map)