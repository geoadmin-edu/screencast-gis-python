# coding: utf-8
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from agro.models import (
    Crop,
    Farm,
    Parcel,
    SoilType
)


class Command(BaseCommand):
    help = 'Create crops'

    def add_arguments(self, parser):
        parser.add_argument('crop_year', nargs='+', type=int)

    def handle(self, *args, **options):
        for crop_year in options['crop_year']:
            Crop.objects.create(name=int(crop_year),
                                start_date=datetime(year=crop_year, month=1, day=1),
                                end_date=datetime(year=crop_year, month=12, day=31))