# coding: utf-8
import os
from datetime import datetime
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import GEOSGeometry
from django.core.management.base import BaseCommand
from agro.models import (
    Crop,
    Farm,
    Parcel,
    SoilType
)


class Command(BaseCommand):
    help = 'Loads agro shapefiles'

    def add_arguments(self, parser):
        parser.add_argument('crop_year', type=int, default=datetime.now().year)
        parser.add_argument('entity', type=str, default='farm')
        parser.add_argument('path', type=str)
        parser.add_argument('-s', '--soil', type=str, dest='soil')
        parser.add_argument('-g', '--grid-type', type=str, dest='grid_type')

    def load_farm(self, crop, path):
        ds = DataSource(path)
        fazenda = ds[0]

        for feat in fazenda:
            farm = Farm()
            farm.geom = GEOSGeometry(feat.geom.wkt, srid=4674)
            farm.id = feat.get("CD_UPNIVEL")
            farm.name = feat.get("DE_UPNIVEL")
            farm.property_type = 'PARTNER'
            farm.adm_unit = feat.get('MOD_ADM')
            farm.crop_id = crop.pk
            farm.save()
            self.stdout.write(self.style.SUCCESS('Farm %s loaded with success' % farm.pk))

    def load_parcel(self, crop, soil, path):
        ds = DataSource(path)
        talhoes = ds[0]

        for feat in talhoes:
            talhao = Parcel()
            talhao.id = feat.get("CD_UPNIV_2")
            talhao.farm_id = feat.get("CD_UPNIVEL")
            talhao.crop_id = crop.pk
            talhao.soil_type_id = soil.id
            talhao.geom = GEOSGeometry(feat.geom.wkt, srid=4674)
            talhao.save()
            self.stdout.write(self.style.SUCCESS('Parcel %s loaded with success' % talhao.pk))

    def handle(self, *args, **options):
        crop_year = int(options.get('crop_year', datetime.now().year))
        entity = options.get('entity')
        path = options.get('path')
        soil_type = options.get('soil')

        if not os.path.isfile(path):
            self.stdout.write(self.style.ERROR('path does not exist'))

        if not entity or not path:
            self.stdout.write(self.style.ERROR('Cannot load data without the option entity or path'))

        if entity == 'parcel' and not soil_type:
            self.stdout.write(self.style.ERROR('Cannot load parcels without the option -s (soil)'))
            return


        try:
            crop = Crop.objects.get(name=crop_year,
                                    start_date=datetime(year=crop_year, month=1, day=1),  # noqa
                                    end_date=datetime(year=crop_year, month=12, day=31))  # noqa
        except Crop.DoesNotExist:
            crop = Crop.objects.create(name=crop_year,
                                       start_date=datetime(year=crop_year, month=1, day=1),  # noqa
                                       end_date=datetime(year=crop_year, month=12, day=31))  # noqa

        if entity == 'farm':
            self.load_farm(crop, path)

        if entity == 'parcel':
            try:
                soil = SoilType.objects.get(name=soil_type)
            except SoilType.DoesNotExist:
                soil = SoilType.objects.create(name=soil_type)

            self.load_parcel(crop, soil, path)

        self.stdout.write(self.style.SUCCESS('%s load complete' % entity))
