# coding: utf-8
from django.conf import settings


def create_agro_layers(apps, schema_editor):
    from django_tilestache.models import Layer
    from django_tilestache.compilers import VecTilesQueryCompiler
    from agro.models import Farm, Parcel

    compiler = VecTilesQueryCompiler()
    db = settings.DATABASES['default']

    try:
        layer = Layer.objects.get(name='farms')
    except:
        layer = Layer()

    layer.name = 'farms'
    layer.provider = {
        'class': 'TileStache.Goodies.VecTiles:Provider',
        'kwargs': {
            'dbinfo': {
                'host': db['HOST'],
                'user': db['USER'],
                'password': db['PASSWORD'],
                'database': db['NAME']
            },
            'clip': False,
            'queries': [
                compiler.compile(
                    Farm.objects.all(),
                    '__all__'
                )
            ]
        }
    }
    layer.cache_lifespan = 600
    layer.allowed_origin = '*'

    layer.save()

    try:
        layer = Layer.objects.get(name='parcels')
    except:
        layer = Layer()

    layer.name = 'parcels'
    layer.provider = {
        'class': 'TileStache.Goodies.VecTiles:Provider',
        'kwargs': {
            'dbinfo': {
                'host': db['HOST'],
                'user': db['USER'],
                'password': db['PASSWORD'],
                'database': db['NAME']
            },
            'clip': False,
            'queries': [
                compiler.compile(
                    Parcel.objects.all(),
                    '__all__'
                )
            ]
        }
    }
    layer.cache_lifespan = 600
    layer.allowed_origin = '*'
    layer.save()
