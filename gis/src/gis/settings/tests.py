# coding: utf-8
# flake8: noqa
from .base import *
from .utils import get_env_variable

SECRET_KEY = 'test'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': get_env_variable("PGDATABASE"),
        'USER': get_env_variable("PGUSER"),
        'PASSWORD':  get_env_variable("PGPASS"),
        'HOST': get_env_variable('DBHOST'),
        'PORT': '5432',
    }
}

INSTALLED_APPS += ('test_without_migrations',)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    SPATIALITE_LIBRARY_PATH = get_env_variable('SPATIALITE_LIBRARY_PATH')
except:
    SPATIALITE_LIBRARY_PATH = 'mod_spatialite'