# coding: utf-8
import os
import sys
import logging
from .utils import get_env_variable


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    # 3rd party apps
    'rest_framework',
    'reversion',
    'rest_framework_gis',
    'django_tilestache',
    'common',
    # our apps
    'agro',
)

DEFAULT_SRID = 4674

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'reversion.middleware.RevisionMiddleware',
)

ROOT_URLCONF = 'gis.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, "templates"),
            os.path.join(BASE_DIR, "..", "..", "..", "gis-app"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'gis.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'gis',
        'USER': get_env_variable("PGUSER"),
        'PASSWORD': get_env_variable("PGPASS"),
        'HOST': "localhost",
        'PORT': "5432",
    }
}

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '..', '..', '..', 'gis-app'),
)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_INTERMEDIARY = os.path.join(BASE_DIR, "..", "..", "..", "gis-static")
STATIC_ROOT = os.path.abspath(os.path.join(STATIC_INTERMEDIARY, "static"))
MEDIA_ROOT = os.path.abspath(os.path.join(STATIC_INTERMEDIARY, "media"))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'addresses': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    },
}

if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)

try:
    from .drf import *
except ImportError:
    pass

WORKFLOW_AUTO_LOAD = True

SITE_ID = 1
