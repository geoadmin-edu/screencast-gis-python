# coding: utf-8
from rest_auth.serializers import PasswordResetSerializer


class PasswordReset(PasswordResetSerializer):

    def get_email_options(self):

        return {
            'email_template_name': 'emails/password_reset_email.html'
        }