from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter
from django.conf.urls import include, url
from django_tilestache.viewsets import LayerViewSet
from django_tilestache.views import (
    TileStacheConfiguration,
    TileStacheTile,
)
from agro.viewsets import (ParcelViewSet,
                           FarmViewSet,)

router = DefaultRouter(trailing_slash=False)
router.register(r'parcels', ParcelViewSet)
router.register(r'farms', FarmViewSet)
router.register(r'layers', LayerViewSet)

urlpatterns = [
    url(
        r'^api/',
        include(router.urls)
    ),
    url(
        r'^api/tilestache/$',
        TileStacheConfiguration.as_view(),
        name='tilestache-configuration'
    ),
    url(
        r'^api/tilestache/(?P<layer_name>[-\w]+)/(?P<z>\d+)/(?P<x>\d+)/(?P<y>\d+).(?P<extension>\w+)',
        TileStacheTile.as_view(),
        name='tilestache-tile'
    ),
    url(r"^$", TemplateView.as_view(template_name="index.html"), name="home"),
]
