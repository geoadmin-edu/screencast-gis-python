# Sistemas de Informação Geográfica

Este é um tema vasto e pretendo falar sobre alguns conceitos como: Datum, Sistemas de Coordenadas, Projeção, Sistema de Referência e conceitos de SIG (Sistemas de Informações Geográficas).
Não conseguiremos cobrir os conceitos de operações espaciais, porém na demonstração isto será ilustrado em linhas gerais.

Este é um artigo derivado do screen cast [SIG com Python](https://youtu.be/M5_VEHQQLzw), disponível no youtube. 

O ambiente em que executamos esta aplicação está disponível no servidor de código fonte da sigma em: https://gitlab.sigmageosistemas.com.br/screencasts/gis-python, neste repositório você tem o Dockerfile e no repositório https://gitlab.sigmageosistemas.com.br/dev/dockerize você encontra instruções de como buildar e montar esta imagem.

## Localização e Medições

Localizar e Medir são uma necessidade inerente ao ser humano desde as sociedades mais antigas.

Com o passar dos séculos, foi preciso definir medidas mais precisas e padronizá-las permitindo às pessoas em qualquer local do planeta, entender e realizar medições, para estimar distancias, áreas e locais, definindo precisamente as fronteiras de um pais, estado, cidade, fazendas e terrenos.

A Geodésia é a área de conhecimento que estuda e aprimora os métodos de medição do planeta.

O planeta terra tem forma similar à uma batata, sendo sua forma matemática mais aproximada, uma elipse, sendo assim, o primeiro passo para calcular as medidas do planeta é a definição de uma elipsoide de referência.

Devido a deformidade do planeta, a forma eliptica se encaixa melhor em variadas regiões, conforme alteramos os parâmetros de tamanho e posição deste em relação ao eixo da Terra. A estes parâmetros que definimos para o elipse chamamos de Datum.

Com base no Datum, podemos determinar em radianos o posicionamento de algo no globo terrestre, ou até mesmo em métros.
Porém quando precisamos representar esta informação em um mapa, em outras palavras, precisamos transformar uma bola em uma folha de papel, é necessário projetar do dado geográfico, projeção é um modelo matemático com base no datum e sistemas de coordenadas, que tranforma as coordenadas geográficas em um plano cartesiano.

A projeção tem diversos tipos:

Cilindricas (Normal, Transversa, Obliqua), Planas (Polar, Equatorial, Obliqua), além de outros tipos como Gnomonica, Estereográfica e etc.

Cada uma delas é ideal para determinadas regiões ou cenários de uso.


## O que é um Sistema de Informação Geográfica (SIG)

É um sistema computacional que utiliza dados de localização geográfica como: coordenadas ou endereços, em conjunto com algoritmos que consumam estes dados para contruir informações.
Não necessáriamente um sistema gis precisa exibir um mapa, ele pode por exemplo informar o tempo estimado para uma entrega ser concluída ou enviar um sms indicando que seu bairro passará por uma instabilidade elétrica.

## Principais pontos ao pensar para construir um SIG

### Qual é o sistema de referência de seu dado geográfico

Por mais que para os desenvolvedores de sistema, este seja uma assunto muito complexo, é extremamente importante ter o conhecimento sobre alguns detalhes do dado geográfico utilizado.

A parte mais importante à se pensar ao criar um sistema de informação geográfica é qual sistema de referência vamos utilizar. Mas como escolher?

Respondendo as questões:
 - Qual abrangência da área de trabalho, cidade, estado, países?
 - Qual principal requisito: precisão de área e comprimento ou precisão de posicionamento?

A resposta destas duas questões é geralmente respondida pela equipe cartográfica de seu cliente ou por geográfos, porém é importante compreender os motivadores destas escolhas.




### Qual a forma de obtenção das coordenadas

Para sistemas que requerem precisão é necessário coletar as informações geográficas com equipamentos avançados, que possuam precisão métrica (ou submetrica) e permitam seu pós processamento, aumentando ainda mais sua precisão.

Para casos em que isso não é necessário, somente as coordenadas gps de um smartphone são suficientes.

Mas tenha em mente que toda iteração (ou interação) com o mapa para criar dados geográficos à mão livre é imprecisa, pois o nível de zoom e a referência pixels para coordenada geográfica prejudicam a precisão.


### Qual é a informação que será produzida com este dado geográfico


É muito importante pensar nisso quando vai contruir um sistema, pois ter uma mapa em sua aplicação a enriquece bastante, mas se não há uma utilidade funcional isso pode acabar tornando-se um grande problema.

Então é crucial pensar no resultado final, ou na informação que queremos produzir e mostrar aos nossos usuários.


## Demo

A demonstração vai exibir código que permite importar dados de um arquivo shp, em seguida como apresentá-los no mapa e por fim sua consulta.

Shapefile: Formato binário de arquivos espaciais da ESRI, que se tornou bastante popular.
Postgis: Banco de dados geográfico.
GEOS: Biblioteca de Operações Espaciais e Conversões;
TileStache: Servidor GIS de Vector Tiles;
Django TileStache: APP Django que cria um endpoint tilestache para suas entidades geográficas.
Django Rest Framework: Permite a criação de endpoints rest para seus django models.
Django Rest Framework GIS: Permite a serialização de Atributos geométricos e operações espaciais através do DRF endpoint;


##Referências:

UFABC Flavia Feitosa: https://flaviafeitosa.files.wordpress.com/2015/06/02a_sistreferencia-compressed.pdf
Geodésia: https://pt.wikipedia.org/wiki/Geod%C3%A9sia
Curso de Postgis Sigma: https://github.com/sigma-geosistemas/sigma.universidade.postgis/wiki/aula02
imagem qgis: https://flic.kr/p/UGGiGu