var $ = require('jquery');
window.$ = $;
window.jQuery = $;
$.fn.poshytip={defaults:null};

require('semantic-ui-css/semantic.css');
require('select2/dist/css/select2.css');
require('leaflet/dist/leaflet.css');
require('leaflet.awesome-markers/dist/leaflet.awesome-markers.css');
require('leaflet-minimap/dist/Control.MiniMap.min.css');
require('x-editable/dist/jquery-editable/css/jquery-editable.css');
require('jsondiffpatch/public/formatters-styles/html.css');
require('jsondiffpatch/public/formatters-styles/annotated.css');
require('font-awesome/css/font-awesome.css');
require('semantic-ui-range/range.css');
require('spectrum-colorpicker/spectrum.css');

var Polyglot = require('polyglot');
var Radio = require('backbone.radio');
var xeditable = require('xeditable');
var Bb = require('backbone');

$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.ajaxOptions = {type: 'PUT'};
// // todo: MIGRAR ISTO PARA UM TEMPLATE A PARTE
$.fn.editableform.template = '<form class="ui form form-inline editableform">' +
                             '<div class="control-group">' +
                             '<div>' +
                             '<div class="editable-input field"></div>' +
                             '<div class="ui buttons editable-buttons"></div>'+
                             '</div>' +
                             '<div class="editable-error-block"></div>' +
                             '</div>' +
                             '</form>';

var Mn = require('backbone.marionette');

$(document).ready(function() {
    Radio.channel('configuration').on('done', function() {
        var locale = localStorage.getItem('locale') || 'pt';
        localStorage.setItem('locale', locale);
        $.getJSON('/static/locales/' + locale + '.json')
        .done(function(data) {
            window.polyglot = new Polyglot({phrases: data});
            window.templateHelpers = require('gis/utils/TemplateHelpersUtils');

            var gis = require('gis/application/Gis');
            var instance = new gis();

            instance.start();
        });
    });
    var Configuration = require('gis/application/Configuration');
});
