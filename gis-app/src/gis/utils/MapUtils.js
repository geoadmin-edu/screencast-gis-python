'use strict';
var L = require('leaflet');
var Mn = require('backbone.marionette');


module.exports = Mn.Object.extend({
    initialize: function(options) {
        this.map = options.map;
    },
    channelName: 'map',
    radioEvents: {
        'flash': 'flash',
        'zoomTo': 'zoomTo',
        'hover': 'hover'
    },
    geoJSONFromModel: function(data, options) {
        options = options || { geometryField: 'geom'};
        return _.pluck(data, options.geometryField);
    },
    flash: function(features, options) {
        options = options || { 
            zoomTo: true,
            flashTimeout: 3000,
            flashColor: '#FF0',
            flashWeight: 1
        };

        var feats = this.geoJSONFromModel(features);
        
        if (options.zoomTo) {
            this.zoomTo(feats, {"formatFeatures": false});
        }
        
        if(feats[0].type === "Polygon"){
            var flashLayer = L.geoJSON(feats, {
                color: '#FF0',
                weight: 1
            });
        }else{
            var geojsonMarkerOptions = {
                radius: 8,
                fillColor: "#FF0",
                color: "#FF0",
                weight: 5,
                opacity: 1,
                fillOpacity: 0.8
            };
            var flashLayer =  L.geoJSON(feats, { pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, geojsonMarkerOptions);}
            })
        }

        this.map.addLayer(flashLayer);
        
        setTimeout(function() {
            flashLayer.remove();
        }, options.flashTimeout);
    },
    zoomTo: function(features, options) {
        options = options || { 
            formatFeatures: true
        };
        
        var feats = features;
        if (options.formatFeatures) {
            feats = this.geoJSONFromModel(features);
        }
        var boundary = L.geoJSON(feats);
        this.map.fitBounds(boundary.getBounds());
    },
    hover: function(features, options) {
        var self = this;
        options = options || {
            flashColor: '#FF0',
            flashWeight: 1
        };

        if (self.hoverLayer) {
            self.hoverLayer.remove();
        }   

        if (features) {
            var feats = this.geoJSONFromModel(features);
            self.hoverLayer = L.geoJSON(feats, {
                color: options.flashColor,
                weight: options.flashWeight
            });
            self.map.addLayer(this.hoverLayer);
        }
    }
});
