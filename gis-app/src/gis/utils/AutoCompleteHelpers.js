"use strict";
module.exports = {
    setInitialOptions: function(select, textAttribute, data) {
        var options = _.map(data.models, function(item) {
            var o = new Option(item.get(textAttribute), item.id);
            o.selected = true;
            return o;
        });

        $(select).append(options);
        $(select).trigger('change');
    },
    actions: {
        regular: function (action) {
            if (!action.id) {
                return action.text;
            }
            return $('<span>' + action.name + '</span>');
        },
        selection: function(action) {
            return action.name || action.text;
        }
    },
    users: {
        regular: function (user) {
            if (!user.id) {
                return user.text;
            }
            return $('<span>' + user.username + '</span>');
        },
        selection: function(user) {
            return user.username || user.text;
        }
    },
    methods: {
        regular: function(method) {
            if (!method.id) {
                return method.text;
            }
            return $('<span>' + method.description + '</span>');
        },
        selection: function(method) {
            return method.description || method.text;
        }
    },
    availableTasks: {
        regular: function(task) {
            if (!task.id) {
                return task.text;
            }

            return $('<span>' + task.name + '</span>');
        },
        selection: function(task) {
            return task.name || task.text;
        }
    },
    machines: {
        regular: function(machine) {
            if (!machine.id) {
                return machine.text;
            }
            return $('<span>' + machine.name + '</span>');
        },
        selection: function(machine) {
            return machine.name || machine.text;
        }
    },
    permissions: {
        regular: function(permission) {
            if (!permission.id) {
                return permission.text;
            }

            return $('<span>' + permission.name + '</span>');
        },
        selection: function(permission) {
            return permission.name || permission.text;
        }
    },
    categories: {
        regular: function(category) {
            if (!category.id) {
                return category.name;
            }

            return $('<span>' + category.name + '</span>');
        },
        selection: function (category) {
            return category.name || category.text;
        }
    },
    sources: {
        regular: function(source) {
            if (!source.id) {
                return source.name;
            }

            return $('<span>' + source.name + '</span>');
        },
        selection: function (source) {
            return source.name || source.text;
        }
    },
    brands: {
        regular: function(brand) {
            if (!brand.id) {
                return brand.full_name;
            }
            return $('<span>' + brand.full_name + '</span>');
        },
        selection: function(brand) {
            return brand.full_name || brand.text;
        }
    }
};
