'use strict';
var Moment = require('moment');

module.exports = {
    formatDateRF: function(date, settings) {
        if (!date) return '';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + month + '-' + day;
    },
    DateNow: function() {
      return Moment().format('YYYY-MM-DD');
    }

};
