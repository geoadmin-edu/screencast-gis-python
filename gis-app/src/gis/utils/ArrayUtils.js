module.exports = {
    pairwise: function(iterable) {
        var result = [];
        for (var i = 0; i<= iterable.length-1;i++) {
            if (i+1 >= iterable.length) {
                break;
            }
            var firstItem = iterable[i];
            var secondItem = iterable[i+1];

            result.push([firstItem, secondItem]);
        }
        return result;
    }
}

