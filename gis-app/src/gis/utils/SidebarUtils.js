var $ = require('jquery');
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');

module.exports = Mn.Object.extend({
    channelName: 'sidebarUtils',
    radioEvents: {
        'hideAll': 'hideAll'
    },
    hideAll: function() {
        $('.ui.sidebar.rightsiderbar')
            .sidebar({
                context: '#main-container .bottom.segment'
            })
            .sidebar('hide');
    }
});
