'use strict';
var _ = require('underscore');
var Moment = require('moment');

module.exports = {
    renderPositive: function(value) {
        if (value === null || value === undefined) {
            return 'red';
        }
        return 'green';
    },
    renderNegative: function(value, color) {
        if (value === null || value === undefined) {
            return 'green';
        }
        return color || 'red';
    },
    renderRankLabel: function(value) {
        var template = _.template('<div class="ui label <%= color %>"><%= value %></div>');
        value = (value * 100).toFixed(2);
        var color = 'green';
        if (value > 0 && value < 26) {
            color = 'red';
        }

        if (value > 25 && value < 51) {
            color = 'orange';
        }

        if (value > 50 && value < 76) {
            color = 'yellow';
        }

        return template({value: value, color: color});
    },
    valueOrDefault: function(val, def) {
        if (val !== null && val !== undefined) {
            return val;
        }

        return def;
    },
    renderDate: function(value) {
        var locale = localStorage.getItem('locale') || 'en';
        if (locale === 'en') {
            return Moment(value).format('YYYY-MM-DD');
        }else{
            return Moment(value).format('DD-MM-YYYY');
        }
    },
    isTrue: function(condition, thenTrue, thenFalse) {
        return condition ? thenTrue : thenFalse;
    }
};
