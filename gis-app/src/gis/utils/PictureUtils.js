const L = require('leaflet');
const P4C = require('pic4carto');
module.exports = {
    drawPictures: function(map, coordinate, radius) {
        var picman = new P4C.PicturesManager();
        picman.on("picsready", function(pictures) {
            _.each(pictures, function(picture) {
                var pictureLayer = L.circle([picture.coordinates.lat, picture.coordinates.lng], {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0.3,
                    radius: 5
                }).addTo(map);

                pictureLayer.bindPopup('<a href="' + picture.pictureUrl + '" target="_blank"><img src="' + picture.pictureUrl + '" style="width: 300px;"/></a>');
            });
        });

        var bounds = L.circle(coordinate, { radius: radius }).addTo(map).getBounds();
        
        //Call picture retrieval function
        picman.startPicsRetrieval(
            new P4C.LatLngBounds(
                new P4C.LatLng(bounds._southWest.lat, bounds._southWest.lng),
                new P4C.LatLng(bounds._northEast.lat, bounds._northEast.lng)
            )
        );
    }
}