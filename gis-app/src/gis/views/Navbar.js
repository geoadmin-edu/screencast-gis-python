'use strict';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');

module.exports = Mn.View.extend({
    template: require('gis/templates/Navbar.html'),
    onAttach: function() {
        $('.ui.menu .ui.dropdown').dropdown();
    },
    events: {
        'click #home': function(event) {
            Bb.history.navigate('', true);
        },
        'click #about': function(event) {
            Bb.history.navigate('about/', true);
        },
        'click #brand-list': function(event) {
            Bb.history.navigate('brands/', true);
        },
        'click #source-list': function(event) {
            Bb.history.navigate('sources/', true);
        },
        'click #collection-method-list': function(event) {
            Bb.history.navigate('methods/', true);
        },
        'click #category-list': function(event) {
            Bb.history.navigate('categories/', true);
        },
        'click #project-list': function(event) {
            Bb.history.navigate('projects/', true);
        },
        'click #user-list': function(event) {
            Bb.history.navigate('users/', true);
        },
        'click #group-list': function(event) {
            Bb.history.navigate('groups/', true);
        },
        'click #machine-list': function(event) {
            Bb.history.navigate('machines/', true);
        },
        'click #state-list': function(event) {
            Bb.history.navigate('states/', true);
        },
        'click #transition-list': function(event) {
            Bb.history.navigate('transitions/', true);
        },
        'click #task-list': function(event) {
            Bb.history.navigate('tasks/', true);
        },
        'click #i18n-en': function(event) {
            Radio.channel('i18n').trigger('change-i18n', 'en');
        },
        'click #i18n-pt': function(event) {
            Radio.channel('i18n').trigger('change-i18n', 'pt');
        },
        'click #nutrients-list': function(event) {
            Bb.history.navigate('samples/nutrient/', true);
        },
        'click #measure-units-list': function(event) {
            Bb.history.navigate('agro/measure_units/', true);
        }
    }
});
