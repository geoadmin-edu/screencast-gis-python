'use strict';
var $ = require('jquery');
var _ = require('underscore');
var L = require('leaflet');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');

var AwesomeMarkers = require('awesomemarkers');
var MiniMap = require('minimap');
var PictureUtils = require('gis/utils/PictureUtils');

var MapUtils = require('gis/utils/MapUtils');

var SelectionContext = require('gis/controls/SelectionContext');
var SelectControl = require('gis/controls/Select');
var ActiveLayer = require('gis/controls/ActiveLayer');
var ResultPanel = require('gis/controls/ResultPanel');

module.exports = Mn.View.extend({
    id: 'map',
    template: require('gis/templates/Map.html'),
    initialize: function(options) {
        var self = this;
        this.layers = options.layers || require('gis/map/Default');

        Mn.View.prototype.initialize.apply(this, arguments);

        Radio.channel('map').on('selectionLimit', function() {
            Radio.channel('message').trigger('warning', polyglot.t('geo-query-limit-message'));
        });
        Radio.channel('map').on('addCoords', function(coords, icon, color) {
            self.addMarkerToMap(coords, icon, color);
        });
        Radio.channel('map').on('addPopup', function(coords, text) {
            self.addPopupToMap(coords, text);
        });
        Radio.channel('map').on('showPictures', function(coordinate, radius) {
            PictureUtils.drawPictures(self.map, coordinate, radius);
        });
        Radio.channel('map').on('enableSelection', function() {
            self._SelectControl.enable();
        });

        this._SelectControl = new SelectControl();
    },

    baseMap: {
        'id': 'mapbox.streets',
        'name': 'gis',
        'url': 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2VvcmdlLXNpbHZhIiwiYSI6Ii1fcGlpUU0ifQ.5exkucINd7OeX4S2DYGx_w'
    },
    layers: [
    ],
    onAttach: function() {
        var self = this;

        var zoomCords = this.options.zoomCords ? this.options.zoomCords : [-20.54749, -48.20484];
        this.map = L.map('map', {
            zoomControl: false,
            center: zoomCords,
            zoom: 15,
            contextmenu: true,
            contextmenuWidth: 200,
            contextmenuItems: []
        });

        this._SelectionContext = new SelectionContext({
            'map': this.map
        });

        this.MapUtils = new MapUtils({
            'map': this.map
        });

        var overlays = {};
        var layerSetup = {};
        for (var key in layerSetup) {
            overlays[key] = layerSetup[key]();
        }
        var _BaseMap = new L.TileLayer(this.baseMap.url, {
            attribution: this.baseMap.name,
            id: this.baseMap.id
        });

        this.map.addLayer(_BaseMap);
        this.map.fire('zoomend');

        L.control.scale().addTo(this.map);
        this.layerControl = L.control.layers({
            'OpenStreetMap': _BaseMap
        }, null, {position: 'bottomleft'});
        this.map.addControl(this.layerControl);

        this.layers.each(function(layer) {
            var leafletLayer = layer.layer;
            self.layerControl.addOverlay(leafletLayer, layer.get('alias'));
        });

        if (this.options.miniMap) {
            var miniBaseMap = new L.TileLayer(this.baseMap.url, {
                minZoom: 0,
                maxZoom: 17,
                attribution: this.baseMap.name,
                id: this.baseMap.id
            });

            var miniMap = new L.Control.MiniMap(miniBaseMap, {
                toggleDisplay: true
            }).addTo(this.map);
        }

        var _ActiveLayer = new ActiveLayer({
            layers: this.layers,
            onChange: $.proxy(function(layerName) {
                var selectedLayer = self.layers.findWhere({name: layerName});
                var collectionClass = selectedLayer.get('collection');
                var collection = new collectionClass();
                this._SelectControl.success = $.proxy(collection.selectByGeometry, collection);
            }, this)
        });

        var _ResultPanel = new ResultPanel({
            position: 'topleft',
            layers: this.layers
        });

        this.map.addControl(_ActiveLayer);
        this.map.addControl(this._SelectControl);
        this.map.addControl(_ResultPanel);

        _ActiveLayer.setUp();
    },

    addMarkerToMap: function(coords, icon, color) {
        if (!icon)
            icon = 'asterisk';
        if (!color)
            color = 'blue';

        self = this;
        _.each(coords, function(item) {
            L.marker([item.latitude, item.longitude], {
                'icon': L.AwesomeMarkers.icon({
                    'icon': icon,
                    'prefix': 'fa',
                    'markerColor': color
                })
            }).addTo(self.map);
        });
    },

    addPopupToMap: function(coords, text) {
        var popup = L.popup({
            'offset': new L.Point(0, -33)
        }).setLatLng(coords)
            .setContent(text)
            .openOn(this.map);
    },

    zoomToGeoJSON: function(geoJSON) {
        if (geoJSON) {
            var boundary = L.geoJSON(geoJSON);
            this.map.fitBounds(boundary.getBounds());
        }
    },
    
    geoJSONFromModel: function(data) {
        return _.pluck(data, 'geom');
    }
});
