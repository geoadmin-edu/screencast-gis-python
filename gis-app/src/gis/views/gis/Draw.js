    "use strict";
    var Draw = require('leaflet-draw');
    var _Control = function(RedLines) {
        module.exports = new L.Control.Draw({
            edit: {
                featureGroup: RedLines
            },
            draw: {
                polyline: {
                    shapeOptions: {
                        color: '#00FFFD',
                        weight: 4,
                        opacity: 0.5,
                    }
                },
                polygon: {
                    shapeOptions: {
                        color: '#00FFFD',
                        weight: 4,
                        opacity: 0.5,
                        fill: true,
                        fillColor: '#5BCACA',
                        fillOpacity: 0.3
                    }
                },
                circle: {
                    shapeOptions: {
                        color: '#00FFFD',
                        weight: 4,
                        opacity: 0.5,
                        fill: true,
                        fillColor: '#5BCACA',
                        fillOpacity: 0.3
                    }
                },
                rectangle: {
                    shapeOptions: {
                        color: '#00FFFD',
                        weight: 4,
                        opacity: 0.5,
                        fill: true,
                        fillColor: '#5BCACA',
                        fillOpacity: 0.3
                    }
                }
            }
        });
    };

    return {
        'Control' : _Control,
    };
