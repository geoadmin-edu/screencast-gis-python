"use strict";
var Bb = require('backbone');
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');
var BaseModalView = require('gis/views/base/BaseModal');

module.exports = Mn.View.extend({
    tagName: 'tr',
    events: {
        'click #detail': 'detail',
        'click #zoomTo': 'zoomTo',
        'mouseenter': 'mouseenter',
        'mouseleave': 'mouseleave',
    },
    detail: function(){
        Bb.history.navigate('agro/parcels/' + this.model.id, true);
    },
    zoomTo: function(){
        Radio.channel('map').trigger('zoomTo', [this.model.attributes]);
    },
    mouseenter: function(){
        var options = {
            flashTimeout: 0,
            flashColor: '#FF0',
            flashWeight: 1,
            "zoomTo": false
        }
        Radio.channel('map').trigger('hover', [this.model.attributes], options);
    },
    mouseleave: function(){
        Radio.channel('map').trigger('hover');
    }
});
