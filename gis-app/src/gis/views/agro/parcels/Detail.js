'use strict';
var $ = require('jquery');
var Bb = require('backbone');
var Radio = require('backbone.radio');

var FormView = require('gis/views/base/BaseForm');

var Parcel = require('gis/models/Parcel');
var Parcels = require('gis/collections/Parcels');

var DetailView = FormView.extend({
    template: require('gis/templates/agro/parcels/Detail.html'),
    initialize: function(model) {
        this.model = model.model;
    },
    submit: function(event) {
        var self = this;
        var data = {};
        FormView.prototype.submit.apply(this, arguments);
        data = this.serializeForm(this.form);
        this.model.save(data,{
            success: function(model, response, options) {
                Radio.channel('message').trigger('info', polyglot.t('save-success'));
            },
            error: function(model, response, options) {
                self.showErrors(response.responseJSON);
            }
        });
    }
});

module.exports = DetailView;