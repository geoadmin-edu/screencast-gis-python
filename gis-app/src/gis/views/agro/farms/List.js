'use strict';
var Mn = require('backbone.marionette');
var BaseListView = require('gis/views/base/BaseList');
var Row = require('gis/views/agro/farms/Row');

var TableBody = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: Row,
    emptyView: require('gis/views/base/BaseEmpty'),
    emptyViewOptions: {
        template: require('gis/templates/EmptyTable.html')
    }
});

var Table = Mn.View.extend({
  tagName: 'table',
  className: 'ui selectable celled padded table',
  template: require('gis/templates/agro/farms/Table.html'),
  initialize: function(options) {
    this.EditMode = options.EditMode ? true : false;
  },
  regions: {
    body: {
      el: 'tbody',
      replaceElement: true
    }
  },

  onRender: function() {
    var _childViewOptions = {
        tagName: 'tr',
        template: require('gis/templates/agro/farms/Row.html'),
        templateContext: {
            'edit_mode': false
        }
    };

    this.showChildView('body', new TableBody({
        collection: this.collection,
        childViewOptions: _childViewOptions
    }));
  }
});

module.exports = Table
