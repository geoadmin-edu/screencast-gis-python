'use strict';
var $ = require('jquery');
var Bb = require('backbone');
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');

var MessageView = Mn.View.extend({
    template: require('gis/templates/Msgbar.html'),
    id: 'msgbar-container',
    className: 'ui center aligned fluid container',
    events: {
      'click #close': 'hide'
    },
    symbols: {
        'error': {
            'symbol': 'remove circle',
            'color': '#f44336'
        },
        'warning': {
            'symbol': 'warning sign',
            'color': '#ff9800'
        },
        'info': {
            'symbol': 'check circle',
            'color': '#2196F3'
        }
    },
    initialize: function(options) {
        this.message = options.message;
        this.type = options.type ? options.type : 'info';
    },
    onAttach: function() {
        $('#msgbar-container').css('background', this.symbols[this.type].color);
        $('#msgbar-icon').addClass(this.symbols[this.type].symbol + ' icon');
        $('#msgbar-container').append(this.message);
    },
    show: function(){
      this.sidebar('show');
    },
    hide: function(){
      this.sidebar('hide');
    },
    sidebar: function(action) {
      $('#main-container .ui.sidebar.msgsidebar').sidebar({
          context: '#main-container .bottom.segment'
      }).sidebar(action);
    }
});

module.exports = Mn.Object.extend({
  channelName: 'message',
  radioEvents: {
    'error': 'showError',
    'info': 'showInfo',
    'warning': 'showWarning',
  },
  showError: function(message) {
    this.showMessage(message, 'error');
  },
  showInfo: function(message) {
    this.showMessage(message, 'info');
  },
  showWarning: function(message) {
    this.showMessage(message, 'warning');
  },
  showMessage: function(message, type) {
    var msgbar = new MessageView({
      'message': message,
      'type': type
    });

    Radio.channel('gis').trigger('renderMessageBar', msgbar);

    msgbar.show();
    window.setTimeout(function() {
      msgbar.hide();
    }, 4000);
  }
});
