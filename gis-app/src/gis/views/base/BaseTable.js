'use strict';
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    tagName: 'table',
    className: 'ui very basic table',
    regions: {
        body: {
            el: 'tbody',
            replaceElement: true
        }
    },
    initialize: function(options) {
        this.tableViewClass = this.getOption('tableViewClass');
        this.model = this.getOption('model');
    },
    onRender: function() {
        this.showChildView('body', new this.tableViewClass({
            collection: this.collection,
            model: this.model
        }));
    }
});
