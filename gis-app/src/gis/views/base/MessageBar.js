'use strict';
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var $ = require('jquery');
var semantic = require('semantic-ui-css/semantic.js');

module.exports = Mn.View.extend({
    className: 'ui yellow message nag',
    template: require('gis/templates/base/MessageBar.html'),
    initialize: function(options) {
        Radio.channel('message').on('show', function(message) {
            $('#info-message').html(message);
            $('.message.nag').nag('show');
            window.setTimeout(function() {
                $('.message.nag').nag('hide');
            }, 7000);
        });
    },
    templateContext: function() {
        return {
            message: this.message,
        };
    }
});
