'use strict';
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');
module.exports = Mn.CollectionView.extend({
    // className: 'ui divided items',
    id: 'cards-list',
    className: 'ui grid',
    initialize: function(options) {
        var self = this;
        Radio.channel('search').on('setFilter', function(data) {
            if (self.collection) {
                var actualData = {};
                _.each(data, function(value, key, list){
                    if (value !== undefined &&
                        value !== null &&
                        value !== '') {
                        actualData[key] = value;
                    }
                });
                self.collection.fetch({
                    data: $.param(actualData)
                });
            }
        });
    },
    emptyView: require('gis/views/base/BaseEmpty')
});
