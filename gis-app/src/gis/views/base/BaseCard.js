"use strict";
var Bb = require('backbone');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var BaseModalView = require('gis/views/base/BaseModal');
module.exports = Mn.View.extend({
    detailRoute: '',
    deleteTitle: '',
    deleteMessage: '',
    className: 'four wide column',
    events: {
        'click #detail': 'detail',
        'click #delete': 'delete',
    },
    initialize: function(options) {
        this.detailRoute = this.getOption('detailRoute');
        this.deleteTitle = this.getOption('deleteTitle');
        this.deleteMessage = this.getOption('deleteMessage');
    },
    detail: function() {

        if (this.detailRoute) {
            Bb.history.navigate(this.detailRoute + this.model.id, true);
        }
    },
    delete: function() {
        var self = this;
        Radio.channel('modal').trigger('open', new BaseModalView({
            model: this.model,
            title: this.deleteTitle,
            message: this.deleteMessage,
            icon: 'trash outline',
            approveCallback: function(context) {
                if (context.hasOwnProperty('model')) {
                    context.model.destroy({
                        wait: true,
                        error: function(model, response) {
                            Radio.channel('modal').trigger('open', new BaseModalView({
                                model: self.model,
                                template: require('gis/templates/base/OkModal.html'),
                                title: polyglot.t('cannot-delete-title.referential-integrity'),
                                message: polyglot.t('cannot-delete-message.referential-integrity'),
                                icon: 'trash outline'
                            }));
                        }
                    });
                }
            }
        }));
    }
});
