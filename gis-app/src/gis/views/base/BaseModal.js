"use strict";
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var $ = require('jquery');
var semantic = require('semantic-ui-css/semantic.js');
module.exports = Mn.View.extend({
    className: 'ui basic modal',
    template: require('gis/templates/base/YesNoModal.html'),
    initialize: function(options) {
        this.model = this.getOption('model');
        this.title = this.getOption('title');
        this.message = this.getOption('message');
        this.icon = this.getOption('icon');
        this.approve = this.getOption('approveCallback') || function() { return true; };
        this.deny = this.getOption('denyCallback') || function() { return true; };
    },
    templateContext: function() {
        return {
            title: this.title,
            message: this.message,
            model: this.model,
            icon: this.icon
        };
    },
    onAttach: function() {
        var self = this;
        $('.ui.basic.modal')
            .modal({
                onApprove: function() {
                    self.approve(self.templateContext());
                },
                onDeny: function() {
                    self.deny(self.templateContext());
                }
            })
            .modal('show');
    }
});
