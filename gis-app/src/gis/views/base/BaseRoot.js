'use strict';
var Bb = require('backbone');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var BaseSearchView = require('gis/views/base/BaseSearch');
var LoadingView = require('gis/views/base/BaseLoading');

module.exports = Mn.View.extend({
    className: 'ui fluid container',
    initialize: function(options) {
        this.viewName = options.viewName || 'Items';
        this.searchViewClass = options.searchViewClass || BaseSearchView;
        this.listViewClass = options.listViewClass;
        this.collectionClass = options.collectionClass;
        this.newRoute = this.getOption('newRoute');
        this.listRoute = this.getOption('listRoute');
    },
    events: {
        'click #new': 'new',
        'click #grid': function() {
            Bb.history.navigate(this.listRoute + 'grid', true);
        },
        'click #table': function() {
            Bb.history.navigate(this.listRoute + 'table', true);
        },
    },
    template: require('gis/templates/base/BaseRoot.html'),
    regions: {
        search: '#search',
        list: '#list'
    },
    templateContext: function() {
        return {
            viewName: this.viewName,
            newRoute: this.newRoute
        };
    },
    onRender: function() {
        var self = this;
        this.showChildView('search', new this.searchViewClass());
        this.showChildView('list', new LoadingView());
        var collection = new this.collectionClass();
        collection.fetch({
            success: function(fullCollection, response, options) {
                self.showChildView('list', new self.listViewClass({
                    collection: fullCollection
                }));
            }
        });

        $('.ui.sticky').sticky({
            context: '#root-content'
        });
    },
    new : function() {
        if (this.newRoute) {
            Bb.history.navigate(this.newRoute, true);
        }
    }
});
