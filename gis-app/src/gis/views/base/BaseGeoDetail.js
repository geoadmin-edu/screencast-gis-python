"use strict";
var _ = require('underscore');
var BaseDetail = require('gis/views/base/BaseDetail');
var MapView = require('gis/views/Map');
module.exports = BaseDetail.extend({
    regions: function() {
        return _.extend(BaseDetail.prototype.regions, {
            map: '#map-detail'
        });
    },
    onAttach: function() {
        this.getRegion('map').empty();
        if (this.model) {
            var mapView = new MapView({
                className: 'small-map',
                draw: false,
                miniMap: false,
                model: this.model
            });
            this.showChildView('map', mapView);
        }
    }
});
