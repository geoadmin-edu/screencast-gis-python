"use strict";
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    className: 'ui main text container',
    template: require('gis/templates/404.html')
});
