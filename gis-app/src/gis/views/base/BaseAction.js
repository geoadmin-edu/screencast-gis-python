"use strict";
var $ = require('jquery');
var semantic = require('semantic-ui-css/semantic.js');
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    template: require('gis/templates/base/BaseAction.html'),
    onAttach: function() {
        $('.menu .item').tab();
    },
});
