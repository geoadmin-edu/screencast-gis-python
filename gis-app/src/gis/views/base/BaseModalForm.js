"use strict";
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var $ = require('jquery');
var semantic = require('semantic-ui-css/semantic.js');
var BaseForm = require('gis/views/base/BaseForm');
module.exports = BaseForm.extend({
    className: 'ui modal',
    initialize: function(options) {
        this.title = this.getOption('title');
        this.approve = this.getOption('approveCallback') || function() { return true; };
        this.deny = this.getOption('denyCallback') || function() { return true; };
        this.geometry = this.getOption('geometry') || null;
    },
    templateContext: function() {
        return {
            'title': this.title,
            'icon': this.icon
        };
    },
    onAttach: function() {
        var self = this;
        $('.ui.modal').modal('show');
    },
    done: function(event) {
        $('.ui.modal').modal('hide');
        BaseForm.prototype.done.apply(this, arguments);
    },
});
