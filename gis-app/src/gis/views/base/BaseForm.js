"use strict";
var _ = require('underscore');
var $ = require('jquery');
var Mn = require('backbone.marionette');

module.exports = Mn.View.extend({
    templateName: '',
    // repeated the extra classes because of how semantic ui works. order matters
    errorTemplate: _.template('<div class="error ui red label"><%- msg %></div>'),
    events: {
        'submit form': 'submit'
    },
    clearErrors: function() {
        $('.error', this.form).remove();
    },
    showErrors: function(errors) {
        var self = this;

        _.map(errors, function(fieldErrors, name) {
            var field = $('#id_' + name + '_error', this.form);

            function appendError(msg) {
                field.html(this.errorTemplate({msg: msg}));
            }
            _.map(fieldErrors, appendError, this);
        }, this);

        if (errors.hasOwnProperty('non_field_errors')) {
            _.map(errors.non_field_errors, function(error){
                $(':button[type="submit"]').before(self.errorTemplate({msg: error}));
            });
        }
    },
    serializeForm: function(form) {
        return _.object(_.map(form.serializeArray(), function(item){
            return [item.name, item.value];
        }));
    },
    submit: function(event) {
        if (event) {
            event.preventDefault();
        }
        this.form = $(event.currentTarget);
        this.clearErrors();
    },
    failure: function(xhr, status, error) {
        var errors = xhr.responseJSON;
        this.showErrors(errors);
    },
    modelFailure: function(model, xhr, options) {
        var errors = xhr.responseJSON;
        this.showErrors(errors);
    },
    done: function(event) {
        if (event) {
            event.preventDefault();
        }

        this.trigger('done');
        this.remove();
    }
});
