"use strict";
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    className: 'ui basic segment',
    template: require('gis/templates/Loading.html')
});
