"use strict";
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    className: 'ui main text container',
    template: require('gis/templates/EmailSent.html'),
    emailStatus: 'green',
    emailTitle: polyglot.t('email.recovery-title'),
    emailMessage: polyglot.t('email.recovery-message'),
    initialize: function(options) {
        this.emailStatus = this.getOption('emailStatus');
        this.emailTitle = this.getOption('emailTitle');
        this.emailMessage = this.getOption('emailMessage');
    },
    templateContext: function() {
        return {
            'emailStatus': this.emailStatus,
            'emailTitle': this.emailTitle,
            'emailMessage': this.emailMessage
        };
    }
});
