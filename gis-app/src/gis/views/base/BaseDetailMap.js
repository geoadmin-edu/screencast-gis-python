'use strict';
var Bb = require('backbone');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var LoadingView = require('gis/views/base/BaseLoading');

module.exports = Mn.View.extend( {
    className: 'ui fluid container left',
    initialize: function(options) {
        this.viewName = options.viewName || 'Items';
        this.listViewClass = options.listViewClass;
        this.collection = options.collection;
        this.selectedLayer = options.selectedLayer;
        this.selectedFeatures = options.selectedFeatures;
    },
    events: {
        'click #zoomAll': 'zoomAll',
    },
    template: require('gis/templates/base/BaseDetailMap.html'),
    regions: {
        detail: '#detail',
        list: '#list'
    },
    templateContext: function() {
        return {
            viewName: this.viewName,
        };
    },
    onRender: function() {
        var self = this;
        self.showChildView('list', new LoadingView());
        if (self.collection) {
            self.features = _.pluck(self.collection.models, 'attributes');
        }
        if (self.listViewClass != undefined) {
            self.showChildView('list', new self.listViewClass({
                collection: self.collection,
                selectedLayer: self.selectedLayer,
                selectedFeatures: self.selectedFeatures
            }));
        }
    },
    zoomAll: function() {
        var self = this;
        Radio.channel('map').trigger('zoomTo', self.features);
    }
});
