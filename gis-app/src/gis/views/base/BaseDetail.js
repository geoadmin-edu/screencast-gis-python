'use strict';
var $ = require('jquery');
var xeditable = require('xeditable');
var Bb = require('backbone');
var Mn = require('backbone.marionette');
var TemplateHelpers = require('gis/utils/TemplateHelpersUtils');

module.exports = Mn.View.extend({
    initialize: function(options) {
        this.actionViewOptions = options;
    },
    className: 'ui main text container content-detail',
    actionViewClass: require('gis/views/base/BaseAction'),
    regions: {
        actions: '#actions',
        history: '#history'
    },
    hoverColor: '#ABC9E1',
    templateContext: function() {
        return {
            model: this.model,
            invalid: this.invalid
        };
    },
    modelEvents: {
        'change': 'updateView'
    },
    updateView: function() {
        this.render();
        if (this.onAttach) {
            this.onAttach();
        }
    },
    configureInlineEdit: function(fields) {
        var self = this;
        _.each(fields, function(value, key) {
            var fieldSelector = ".id_" + key;
            var options = _.extend(value, {
                name: key,
                showbuttons: false,
                highlight: "#2ABA1B",
                pk: self.model.id,
                success: function(response, newValue) {
                    self.model.set(key, newValue);
                    self.model.save();
                },
                error: function(response, newValue) {
                    // TODO: configurar um log de error
                }
            });
            $(fieldSelector).editable(options);
        });
    },
    onRender: function() {
        if (this.actionViewClass) {
            this.showChildView('actions',
                new this.actionViewClass({
                    model: this.model,
                    selectedLayer: this.actionViewOptions.selectedLayer,
                    selectedFeatures: this.actionViewOptions.selectedFeatures
                })
            );
        }
    }
});
