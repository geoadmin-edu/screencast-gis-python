"use strict";
var Mn = require('backbone.marionette');
module.exports = Mn.View.extend({
    className: 'ui main text container',
    template: require('gis/templates/403.html')
});
