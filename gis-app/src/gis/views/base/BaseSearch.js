'use strict';
var FormView = require('gis/views/base/BaseForm');
var Radio = require('backbone.radio');
var $ = require('jquery');
var semantic = require('semantic-ui-css/semantic.js');
module.exports = FormView.extend({
    template: require('gis/templates/base/BaseSearch.html'),
    submit: function() {
        var data = {};
        FormView.prototype.submit.apply(this, arguments);
        data = this.serializeForm(this.form);
        // NECESSÁRIO ARRUMAR ISTO AQUI. ELE TÁ REGISTRANDO E DISPARANDO
        // MIL SIGNALS
        Radio.channel('search').trigger('setFilter', data);
    }
});
