'use strict';
var $ = require('jquery');
var Bb = require('backbone');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var LoadingView = require('gis/views/base/BaseLoading');
var SemanticUIRange = require('semanticUiRange');

module.exports = Mn.View.extend({
    id: 'rank-filter',
    template: require('gis/templates/base/BaseFilter.html'),
    initialize: function(options) {
        this.rankValue = 0.75;
        this.status = null;
        this.matchType = null;
        this.templateContext = {
            'filterType': options.filterType ? options.filterType : '' 
        }
    },
    events: {
        'click #hide': 'hide',
        'click #clear': 'clear',
        'click #filter': 'filter'
    },
    onAttach: function() {
        var self = this;
        $('#city-name').val('');
        $('#rank-filter #range').range({
            min: 0,
            max: 100,
            start: 75,
            onChange: function(val) { 
                self.rankValue = val / 100;
                $('#rank-value').html(self.rankValue.toFixed(2));
            }
        });
        $('#rank-filter .ui.dropdown').dropdown({
            onChange: function(value, text) {
                self.status = value.trim();
            }
        });
        $('#match-type-filter .ui.dropdown').dropdown({
            onChange: function(value, text) {
                self.matchType = value.trim();
            }
        });
        $('#rank-filter .ui.dropdown').dropdown('clear');
    },
    filter: function() {
        var cityName = $('#city-name').val();
        Radio.channel('filter').trigger('doFilter', cityName, this.status, this.matchType, this.rankValue);
        this.hide();
    },
    clear: function() {
        this.onAttach();
    },
    hide: function() {
        $('.ui.sidebar').sidebar('hide');
    }
});
