'use strict';
var $ = require('jquery');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var RootTemplate = require('gis/templates/Root.html');
var NavbarView = require('gis/views/Navbar');
var MessageBar = require('gis/views/Msgbar');
var SidebarUtils = require('gis/utils/SidebarUtils');


module.exports = Mn.View.extend({
    id: 'app',
    template: RootTemplate,
    regions: {
        appSidebar: '#app-sidebar',
        topSidebar: '#top-sidebar',
        msgSidebar: '#msg-sidebar',
        navbar: '#navbar',
        content: '#content',
        modal: '#gis-modal'
    },
    initialize: function() {
        var self = this;

        Radio.channel('modal').on('open', function(view) {
            self.showChildView('modal', view);
        });
        Radio.channel('gis').on('renderSidebar', function(view) {
            self.showChildView('appSidebar', view);
        });
        Radio.channel('gis').on('renderTopSidebar', function(view) {
            self.showChildView('topSidebar', view);
        });
        Radio.channel('gis').on('renderMessageBar', function(view) {
            self.showChildView('msgSidebar', view);
        });
        this.MessageBar = new MessageBar();
    },
    onRender: function() {
        this.renderNavBar();
    },
    onAttach: function() {
        var _sidebarUtils = new SidebarUtils();
    },
    renderNavBar: function() {
        this.showChildView('navbar', new NavbarView());
    }
});
