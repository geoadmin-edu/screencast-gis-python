'use strict';
var BaseController = require('gis/controllers/BaseController');
var MapView = require('gis/views/Map');
var AboutView = require('gis/views/About');
module.exports =  BaseController.extend({
    home: function() {
        this.app.renderContentView(new MapView({
            draw: true,
            miniMap: true
        }));
    },
    about: function() {
        this.app.renderContentView(new AboutView());
    }
});
