'use strict';
var $ = require('jquery');
var Bb = require('backbone');
var Radio = require('backbone.radio');

var BaseController = require('gis/controllers/BaseController');
var ErrorView = require('gis/views/base/BaseError');

var Farm = require('gis/models/Farm');
var Farms = require('gis/collections/Farms');
var DetailViewFarm = require('gis/views/agro/farms/Detail');
var Parcel = require('gis/models/Parcel');
var Parcels = require('gis/collections/Parcels');
var DetailViewParcel = require('gis/views/agro/parcels/Detail');

module.exports = BaseController.extend({
    embeddedDetailFarm: function(modelId) {
        var farms = new Farms();
        farms.getOrFetch(modelId).done(function(model) {
            Radio.channel('gis').trigger('renderSidebar', new DetailViewFarm({
                model: model,
                template: require('gis/templates/agro/farms/Detail.html')
            }));
        }).fail(function(model) {
            Radio.channel('gis').trigger('renderSidebar', new ErrorView());
        });
    },
    embeddedDetailParcel: function(modelId) {
        var parcels = new Parcels();
        parcels.getOrFetch(modelId).done(function(model) {
            Radio.channel('gis').trigger('renderSidebar', new DetailViewParcel({
                model: model,
                template: require('gis/templates/agro/parcels/Detail.html')
            }));
        }).fail(function(model) {
            Radio.channel('gis').trigger('renderSidebar', new ErrorView());
        });
    }
});
