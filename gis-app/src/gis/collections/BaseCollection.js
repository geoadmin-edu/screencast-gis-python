'use strict';
var Bb = require('backbone');
var $ = require('jquery');
var Configuration = require('gis/application/Configuration');

module.exports = Bb.Collection.extend({
    initialize: function(options) {
        Bb.Collection.prototype.initialize.apply(this, arguments);
        if (options && options.url) {
            this.url = options.url;
        }
    },
    url: function() {
        return Configuration.getConfig().data[this.endPointName];
    },
    parse: function(response) {
        this._next = response.next;
        this._previous = response.previous;
        this._count = response.count;
        return response.results || [];
    },
    getOrFetch: function(id) {
        var result = new $.Deferred(),
            model = this.get(id);

        if (!model) {
            model = this.push({id: id});
            model.fetch({
                success: function(model, response, options) {
                    result.resolve(model);
                },
                error: function(model, response, options) {
                    result.reject(model, response);
                }
            });
        }
        else {
            result.resolve(model);
        }

        return result;
    },
});
