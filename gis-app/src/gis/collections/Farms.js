'use strict';
var BaseGeoCollection = require('gis/collections/BaseGeoCollection');
var Farm = require('gis/models/Farm');

module.exports = BaseGeoCollection.extend({
    model: Farm,
    endPointName: 'farms'
});