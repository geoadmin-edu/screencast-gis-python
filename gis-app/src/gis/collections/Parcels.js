'use strict';
var BaseGeoCollection = require('gis/collections/BaseGeoCollection');
var Parcel = require('gis/models/Parcel');

module.exports = BaseGeoCollection.extend({
    model: Parcel,
    endPointName: 'parcels'
});