'use strict';
var BaseCollection = require('gis/collections/BaseCollection');
var Radio = require('backbone.radio');


module.exports = BaseCollection.extend({
    geoQueryLimit: 500,
    geoJSONFromModel: function(data) {
        return _.pluck(data, 'geom');
    },
    selectByGeometry: function(geometry, callback) {
        var self = this;

        this.fetch({
            data: {
                'intersects_geom': geometry,
                'limit': this.geoQueryLimit
            },
            success: function(collection, response, options) {
                var feats = self.geoJSONFromModel(response.results);
                if (response.count >= self.geoQueryLimit){
                    Radio.channel('map').trigger('selectionLimit');
                }
                callback(collection, feats);
            }
        });
    }
});
