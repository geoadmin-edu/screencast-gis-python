'use strict';
var Radio = require('backbone.radio');
var _ = require('underscore');
var L = require('leaflet');
var Mn = require('backbone.marionette');

module.exports = L.Control.extend({
    options: {
        position: 'topleft'
    },
    initialize: function(options) {
        this.enabled = false;
        this._start = false;
        this._1stPair = [];
        this._2ndPair = [];
    },
    enable: function() {
        if (!this.enabled) {
            L.DomEvent
                .on(this._map, 'mousedown', this._onStart, this)
                .on(this._map, 'mouseup', this._onFinish, this)
                .on(this._map, 'mousemove', this._onDraw, this);
            $(this._map._container).css('cursor', 'crosshair');
        } else {
            L.DomEvent
                .off(this._map, 'mousedown', this._onStart, this)
                .off(this._map, 'mouseup', this._onFinish, this)
                .off(this._map, 'mousemove', this._onDraw, this);
            $(this._map._container).css('cursor', '');
        }
        this.enabled = !this.enabled;
    },
    onAdd: function(map) {
        this._map = map;
        return this._createButton();
    },
    onRemove: function(map) {
        this._SelectedLayer.remove();
    },
    _onStart: function(event) {
        this._SelectedLayer = L.rectangle([[0, 0], [1, 1]], {
            color: '#ff7800',
            weight: 1
        });
        this._map.addLayer(this._SelectedLayer);

        this._start = true;
        this._map.dragging.disable();
        this._1stPair = [event.latlng.lat, event.latlng.lng];
        L.DomEvent.stop(event);
    },
    _onFinish: function(event) {
        if (this._2ndPair.length <= 0)
            this._SelectedLayer = L.marker(this._1stPair);

        var _addToSelection = event.originalEvent.ctrlKey;
        L.DomEvent.stop(event);
        var self = this;
        this._start = false;
        this._map.dragging.enable();
        if (this.success) {
            this.success(
                JSON.stringify(this._SelectedLayer.toGeoJSON().geometry),
                function(collection, geoJSONFeatures) {
                    Radio.channel('selection').trigger(
                        'select',
                        collection,
                        geoJSONFeatures,
                        {'addToSelection': _addToSelection });
                }
            );
        }
        this._2ndPair = [];
        this._SelectedLayer.remove();
    },
    _onDraw: function(event) {
        if (this._start) {
            $(this._map._container).css('cursor', 'crosshair');
            this._2ndPair = [event.latlng.lat, event.latlng.lng];
            this._SelectedLayer.setBounds([this._1stPair, this._2ndPair]);
        }
        L.DomEvent.stop(event);
    },
    _createButton: function() {
        var _container = L.DomUtil.create('div', 'ui vertical buttons');
        this._button = L.DomUtil.create('div', 'ui icon blue button', _container);

        var _onClick = function(event) {
            L.DomEvent.stop(event);
            this.enable();
            if (this.enabled) {
                L.DomUtil.addClass(this._button, 'teal');
                L.DomUtil.removeClass(this._button, 'blue');
            } else {
                L.DomUtil.addClass(this._button, 'blue');
                L.DomUtil.removeClass(this._button, 'teal');
            }
        };

        var _icon = L.DomUtil.create(
            'i',
            'large hand pointer icon',
            this._button
        );
        L.DomEvent.on(this._button, 'click', _onClick, this);
        L.DomEvent.on(_icon, 'click', _onClick, this);

        var _stopEventPropagation = function(ev) {
            L.DomEvent.stopPropagation(ev);
        };
        L.DomEvent.on(this._button, 'click', _stopEventPropagation);
        L.DomEvent.on(this._button, 'mousedown', _stopEventPropagation);
        L.DomEvent.on(this._button, 'mouseup', _stopEventPropagation);
        L.DomEvent.on(this._button, 'mousemove', _stopEventPropagation);

        return _container;
    }
});

