'use strict';
var $ = require('jquery');
var _ = require('underscore');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');


module.exports = L.Control.extend({
    options: {
        position: 'topleft'
    },
    initialize: function(options) {
        this.__proto__.initialize(options); // tem outro jeito de fazer isso.
        this.layers = options.layers;
        Radio.channel('active-layer').on('onChange', options.onChange);
    },
    onAdd: function(map) {
        return this._createElement();
    },
    setUp: function() {
        $('#active-layer').dropdown({
            onChange: function(a, b, item) {
                Radio.channel('selection').trigger('clearSelection');
                Radio.channel('active-layer').trigger('onChange', $(item).attr('value'));
                Radio.channel('result-panel').trigger('refresh');
            }
        });
    },
    _createElement: function() {

         var selectActiveLayer = L.DomUtil.create('div', 'ui selection dropdown');
         selectActiveLayer.setAttribute('id', 'active-layer');

         L.DomUtil.create('i', 'dropdown icon', selectActiveLayer);

         var title = L.DomUtil.create('div', 'text', selectActiveLayer);
         title.append('Active Layer');

         var select = L.DomUtil.create('div', 'menu', selectActiveLayer);
         this.layers.each(function(item) {
            var option = L.DomUtil.create('div', 'item', select);
            option.setAttribute('value', item.get('name'));
            option.append(item.get('alias'));
         });

        var _stopEventPropagation = function(ev) {
            L.DomEvent.stopPropagation(ev);
        };
        L.DomEvent.on(selectActiveLayer, 'click', _stopEventPropagation);
        L.DomEvent.on(selectActiveLayer, 'mousedown', _stopEventPropagation);
        L.DomEvent.on(selectActiveLayer, 'mouseup', _stopEventPropagation);
        L.DomEvent.on(selectActiveLayer, 'mousemove', _stopEventPropagation);

        return selectActiveLayer;
    }
});

