'use strict';
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');


module.exports = Mn.Object.extend({
  initialize: function(options) {
    this._map = options.map;
    this._SelectedFeatures = L.geoJSON();
    this._map.addLayer(this._SelectedFeatures);
    this._collection = null;
    this._features = [];
  },
  channelName: 'selection',
  radioEvents: {
    'select': 'selectFeatures',
    'clearSelection': 'unSelectFeatures'
  },
  selectFeatures: function(collection, features, options) {
    if (!options || !options.addToSelection) {
      this._SelectedFeatures.clearLayers();
      this._collection = collection;
      this._features = features;
    } else {
      if (this._collection) {
        this._collection.add(collection.models);
        this._features = this._features.concat(features);
      } else {
        this._features = features;
        this._collection = collection;
      }
    }

    this._SelectedFeatures.clearLayers();
    this._SelectedFeatures.addLayer(L.geoJSON(this._features));

    if (this._features.length > 0) {
        // TODO: unificar os eventos abaixo
        Radio.channel('result-panel').trigger('refresh', this._collection, this._features);
        Radio.channel('selection').trigger('selected', this._collection, this._features);
    }else {
      if (!options || !options.addToSelection) {
        this.unSelectFeatures(features);
      }
    }
  },
  unSelectFeatures: function(features, options) {
    this._collection = null;
    this._features = [];
    this._SelectedFeatures.clearLayers();
    Radio.channel('result-panel').trigger('refresh');
  }
});
