'use strict';
var $ = require('jquery');
var _ = require('underscore');
var Radio = require('backbone.radio');
var Mn = require('backbone.marionette');
var BaseDetailMapView = require('gis/views/base/BaseDetailMap');
var MapLayers = require('gis/map/Default');

module.exports = L.Control.extend({
    options: {
        position: 'topleft'
    },
    initialize: function(options) {
        var self = this;
        this.__proto__.initialize(options);
        this.layers = options.layers;

        Radio.channel('active-layer').on('onChange', this.onLayerActive);
        Radio.channel('selection').off('selected', this.onSelected);
        Radio.channel('selection').on('selected', this.onSelected);
    },
    enable: function() {
        this.enabled = !this.enabled;
    },
    onSelected: function(collection, geoJSONFeatures) {
        self._SelectedFeatures = collection;
    },
    onLayerActive: function(layerName) {
        self._SelectedLayer = MapLayers.findWhere({name: layerName});
    },
    onAdd: function(map) {
        var self = this;
        Radio.channel('result-panel').on(
            'refresh',
            function(collection, geoJSONFeatures) {
                if (collection === null) {
                    L.DomUtil.removeClass(self._label, 'show');
                    L.DomUtil.addClass(self._label, 'hide');
                    self._label.innerHTML = '';
                }else {
                    L.DomUtil.removeClass(self._label, 'hide');
                    L.DomUtil.addClass(self._label, 'show');
                    self._label.innerHTML = geoJSONFeatures ? geoJSONFeatures.length : '0';
                }
                self.collection = collection;
                Radio.channel('gis').trigger('renderSidebar',
                    new BaseDetailMapView({
                        'collection': this.collection,
                        'listViewClass': self._SelectedLayer ? self._SelectedLayer.get('listView') : null,
                        'viewName': self._SelectedLayer ? self._SelectedLayer.get('alias') : '',
                        'selectedLayer': self._SelectedLayer,
                        'selectedFeatures': self._SelectedFeatures
                    })
                );
            }
        );
        return this._createButton();
    },
    _createButton: function() {
        var _container = L.DomUtil.create('div', 'ui vertical buttons');

        var _btnResultPanel = L.DomUtil.create(
            'div',
            'ui icon blue button',
            _container);

        this._label = L.DomUtil.create(
            'div',
            'floating ui red mini label hide',
            _btnResultPanel);

        var _onClickPanel = function(event) {
            if (!this.collection) return;

            L.DomEvent.stop(event);
            this.enable();
            if (this.enabled) {
                L.DomUtil.addClass(_btnResultPanel, 'teal');
                L.DomUtil.removeClass(_btnResultPanel, 'blue');
            } else {
                L.DomUtil.addClass(_btnResultPanel, 'blue');
                L.DomUtil.removeClass(_btnResultPanel, 'teal');
            }
            Radio.channel('gis').trigger('renderSidebar',
                new BaseDetailMapView({
                    'collection': this.collection,
                    'listViewClass': self._SelectedLayer.get('listView'),
                    'viewName': self._SelectedLayer.get('alias'),
                    'selectedLayer': self._SelectedLayer,
                    'selectedFeatures': self._SelectedFeatures
                })
            );

            $('.ui.sidebar.rightsiderbar')
                .sidebar({
                    context: '#main-container .bottom.segment'
                }).sidebar('setting', 'closable', false)
                .sidebar('setting', 'dimPage', false)
                .sidebar('toggle');
        };

        var _icon = L.DomUtil.create(
            'i',
            'large list layout icon',
            _btnResultPanel
        );
        L.DomEvent.on(_btnResultPanel, 'click', _onClickPanel, this);
        L.DomEvent.on(_icon, 'click', _onClickPanel, this);

        var _stopEventPropagation = function(ev) {
            L.DomEvent.stopPropagation(ev);
        };
        L.DomEvent.on(_btnResultPanel, 'click', _stopEventPropagation);
        L.DomEvent.on(_btnResultPanel, 'mousedown', _stopEventPropagation);
        L.DomEvent.on(_btnResultPanel, 'mouseup', _stopEventPropagation);
        L.DomEvent.on(_btnResultPanel, 'mousemove', _stopEventPropagation);

        return _container;
    }
});
