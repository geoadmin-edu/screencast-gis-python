define(function(require) {
    'use strict';
    var $ = require('jquery');
    var Bb = require('backbone');
    var Mn = require('backbone.marionette');
    var Radio = require('backbone.radio');

    var AgroRouter = require('gis/routers/AgroRouter');
    var MainRouter = require('gis/routers/MainRouter');

    var AjaxUtils = require('gis/utils/AjaxUtils');
    var ajaxUtils = new AjaxUtils();
    var RootView = require('gis/views/Root');

    return Mn.Application.extend({
        region: '#app-body',
        initialize: function(options) {
            var self = this;
            this.options = options;
            Radio.channel('view').on('renderView',
                $.proxy(this.renderView, this));
            Radio.channel('gis').on('renderContent', function(view) {
                self.renderContentView(view);
            });
            Radio.channel('view').on('renderContentView',
                $.proxy(this.renderContentView, this));
            Radio.channel('i18n').on('change-i18n', function(locale){
                localStorage.setItem('locale', locale);
                $.getJSON('static/locales/' + locale + '.json', function(data){
                    if (polyglot) {
                        polyglot.replace(data);
                        location.reload(true);
                    }
                });
            });

        },
        onStart: function(options) {
            this.routers = this.initializeRouters({
                agro: AgroRouter,
                main: MainRouter
            });
            this.initializeRootView();
            Bb.history.start({pushState: false});
        },
        initializeRouters: function(routers) {
            var self = this;
            var routerHash = {};
            _.map(routers, function(value, key) {
                routerHash[key] = new value({
                    pushState: true,
                    app: self
                });
            });
            return routerHash;
        },
        initializeRootView: function() {
            window.templateHelpers.app = self;
            this.rootView = new RootView();
            this.rootView.renderNavBar();
            this.showView(this.rootView);
        },
        renderView: function(view) {
            this.showView(view);
        },
        renderContentView: function(view) {
            Radio.channel('sidebarUtils').trigger('hideAll');
            this.rootView.showChildView('content', view);
        }
    });
});
