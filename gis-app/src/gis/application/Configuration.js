define(function(require) {
    'use strict';
    var $ = require('jquery');
    var Mn = require('backbone.marionette');
    var Radio = require('backbone.radio');
    var config = {};

    var Configuration = Mn.Object.extend({
        initialize: function() {
            config.apiRoot = window.apiRoot;
            config.apiLogin = window.apiLogin;

            $.getJSON(config.apiRoot).done(function(data) {
                config.data = data;
                Radio.channel('configuration').trigger('done');
            });
        },
        getConfig: function() {
            return config;
        }
    });
    var configurer = null;

    function getSingleton() {
        if (configurer === null) {
            configurer = new Configuration();
            return configurer;
        }
        return configurer;
    }

    return getSingleton();
});
