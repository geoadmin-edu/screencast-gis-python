"use strict";
var _ = require('underscore');
var Bb = require('backbone')

var extendCollection = function(collection, collectionOptions) {
    collectionOptions = collectionOptions || {};
    if (collectionOptions.fetch) {
        var newCollection = collection.extend({
            fetch: function(options) {
                options = options || {};
                options.data = options.data || {};
                options.data = _.extend({}, options.data, collectionOptions.fetch);
                return collection.prototype.fetch.apply(this, arguments);
            }
        });
        return newCollection;
    }
    return collection;
};

module.exports =  Bb.Model.extend({
    defaults: {
        name: null,
        alias: null, // human readable
        collection: null,
        collectionOptions: null,
        layerFactory: null,
        layerFactoryOptions: null,
        layerOptions: null,
        layerSwitcher: false,
        visible: false,
        listView: null,
        searchView: null,
    },
    initialize: function(options) {
        Bb.Model.prototype.initialize.apply(this, arguments);
        // this is here so we can keep an eye
        // to the reference of this layer.
        // from this we can further manipulate it
        this.layer = this.createLayer();
    },
    createCollection: function() {
        var collectionClass = this.get('collection');
        var collectionOptions = this.get('collectionOptions');
        if (!collectionClass)
            return null;

        if (collectionClass && collectionOptions) {
            return extendCollection(collectionClass, collectionOptions);
        }
        else {
            return collectionClass;
        }
    },
    createLayer: function() {
        var layerFactory = this.get('layerFactory');
        if (layerFactory) {
            var factoryOptions = this.get('layerFactoryOptions') || null;
            var fact = new layerFactory(factoryOptions);
            return fact.createLayer(this);
        }
        return null;
    }
});