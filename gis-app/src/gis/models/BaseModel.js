"use strict";
var Bb = require('backbone');
module.exports =  Bb.Model.extend({
    initialize: function(options) {
        var url = options.url || null;
        if (url !== undefined && url !== null) {
            this.url = url;
        }
    },
    url: function() {
        var links = this.get('links'),
            url = links && links.self;

        if (!url) {
            url = Backbone.Model.prototype.url.call(this);
        }

        return url;
    }
});
