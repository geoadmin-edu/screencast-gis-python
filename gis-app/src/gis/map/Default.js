'use strict';
var PBFLayerFactory = require('gis/map/PBFLayerFactory');
var MapLayers = require('gis/collections/MapLayers');
var MapLayer = require('gis/models/MapLayer');

var Farms = require('gis/collections/Farms');
var FarmListView = require('gis/views/agro/farms/List');

var Parcels = require('gis/collections/Parcels');
var ParcelListView = require('gis/views/agro/parcels/List');

var tileserverUrl = process.env.TILESERVER_URL || 'http://localhost:8000/api/tilestache/';

module.exports = new MapLayers([
    new MapLayer({
        name: 'farms',
        alias: polyglot.t('farms'),
        collection: Farms,
        layerFactory: PBFLayerFactory,
        layerFactoryOptions: {
            baseUrl: tileserverUrl
        },
        layerOptions: {
            minZoom: 8,
            maxZoom: 21,
            style: {
                fill: false,
                weight: 3.5,
                color: '#985D5D',
                opacity: 1
            }
        },
        listView: FarmListView
    }),
    new MapLayer({
        name: 'parcels',
        alias: polyglot.t('parcels'),
        collection: Parcels,
        layerFactory: PBFLayerFactory,
        layerFactoryOptions: {
            baseUrl: tileserverUrl
        },
        layerOptions: {
            minZoom: 9,
            maxZoom: 21,
            style: {
                fill: false,
                weight: 2,
                color: '#B48181',
                opacity: 1
            }
        },
        listView: ParcelListView,
    })
]);
