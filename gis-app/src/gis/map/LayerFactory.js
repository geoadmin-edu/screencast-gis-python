'use strict';
var Mn = require('backbone.marionette');

module.exports = Mn.Object.extend({
    initialize: function(options) {
        Mn.Object.prototype.initialize.apply(this, arguments);
    },
    createLayer: function(layerModel) {
        // inheritors should implement this
    },
    buildLayerOptions: function(options) {
        // notimplemented
    }

});