'use strict';
var _ = require('underscore');
var L = require('leaflet');
var VectorGrid = require('vectorGrid');
var LayerFactory = require('gis/map/LayerFactory');

module.exports = LayerFactory.extend({
    layerOptions: {
        rendererFactory: L.svg.tile,
        zIndex: 0,
        minZoom: 0,
        maxZoom: 21,
        interactive: true,
        vectorTileLayerStyles: {}
    },
    initialize: function(options) {
        LayerFactory.prototype.initialize.apply(this, arguments);
        options = options || { baseUrl: 'http://localhost:8000/api/tilestache/' };
        this.baseUrl = options.baseUrl;
    },
    compileURL: function(name, url, format){
        format = format || 'pbf';
        var fullUrl = this.baseUrl + name + '/{z}/{x}/{y}.' + format;
        return fullUrl;
    },
    buildLayerOptions: function(layerModel) {
        var layerOptions = layerModel.get('layerOptions');
        layerOptions = layerOptions || {};
        var cloned = JSON.parse(JSON.stringify(layerOptions));
        var layerName = layerModel.get('name');
        var vectorStyles = {};
        vectorStyles[layerName] = layerOptions.style;
        cloned.vectorTileLayerStyles = vectorStyles;
        delete cloned.style;
        var newOptions = _.extend({}, this.layerOptions, cloned);
        return newOptions;
    },
    createLayer: function(layerModel) {
        var fullOptions = this.buildLayerOptions(layerModel);
        var fullUrl = this.compileURL(layerModel.get('name'));
        return L.vectorGrid.protobuf(
            fullUrl,
            fullOptions
        );
    },
});
