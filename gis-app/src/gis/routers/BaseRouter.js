'use strict';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
var Radio = require('backbone.radio');
var RootView = require('gis/views/Root');


module.exports =  Mn.AppRouter.extend({
    initialize: function(options) {
        this.app = options.app || null;
        this.publicRoutes = options.publicRoutes || this.publicRoutes;

        if (this.controller) {
            this.controller.app = this.app;
        }

        Radio.channel('active-layer').on('onChange', this.onLayerActive);
        Radio.channel('selection').off('selected', this.onSelected);
        Radio.channel('selection').on('selected', this.onSelected);
    },
    onSelected: function(collection, geoJSONFeatures) {
        self._SelectedFeatures = collection;
    },
    onLayerActive: function(layerName) {
        var MapLayers = require('gis/map/Default');
        self._SelectedLayer = MapLayers.findWhere({name: layerName});
    },
    publicRoutes: [],
    execute: function(callback, args, name) {
        args[args.length - 1] = {
          selectedFeatures: self._SelectedFeatures,
          selectedLayer: self._SelectedLayer
        };

        if (this.publicRoutes && this.publicRoutes.indexOf(name) >= 0) {
            if (callback) callback.apply(this, args);
            return true;
        }

        if (callback) callback.apply(this, args);
        return true;
    }
});
