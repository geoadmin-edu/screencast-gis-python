'use strict';
var Bb = require('backbone');
var BaseRouter = require('gis/routers/BaseRouter');
var Controller = require('gis/controllers/AgroController');
var Radio = require('backbone.radio');

module.exports =  BaseRouter.extend({
    controller: new Controller(),
    permissions: {
      'embeddedDetailFarm': 'agro.change_farm',
      'embeddedDetailParcel': 'agro.change_parcel'
    },
    appRoutes: {
        'agro/farms/:modelId': 'embeddedDetailFarm',
        'agro/parcels/:modelId': 'embeddedDetailParcel'
    }
});
