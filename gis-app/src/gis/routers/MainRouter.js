"use strict";
var BaseRouter = require('gis/routers/BaseRouter');
var MainController = require('gis/controllers/MainController');

module.exports =  BaseRouter.extend({
    controller: new MainController(),
    appRoutes: {
        '': 'home',
        'about/': 'about'
    }
});
