var webpack = require('webpack');
var path = require('path');
module.exports = {
    entry: './src/main.js',
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.png$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.gif$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.jpg$/,
                loader: 'file-loader'
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader'
            }
        ]
    },
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js',
        publicPath: '/static/build/'
    },
    plugins: [
        new webpack.EnvironmentPlugin(
            [
                'ENV',
                'TILESERVER_URL', // if Django, then localhost:8000/api/tilestache/ else http://tileserver/
                'AGRO', // if Django, then DJANGO else REMOTE
            ]
        ),
        new webpack.ProvidePlugin({
            _: 'underscore'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jquery': 'jquery',
            'window.$': 'jquery'
        })
    ],
    resolve: {
        moduleDirectories: [__dirname + '/node_modules', __dirname + '/bower_components'],
        root: __dirname + '/src',
        alias: {
            polyglot: 'node-polyglot/index',
            xeditable: 'x-editable/dist/jquery-editable/js/jquery-editable-poshytip.js',
            semanticUiRange: 'semantic-ui-range/range.js',
            jquery: 'jquery/dist/jquery',
            underscore: 'underscore',
            minimap: 'leaflet-minimap/dist/Control.MiniMap.min.js',
            awesomemarkers: 'leaflet.awesome-markers/dist/leaflet.awesome-markers.js',
            jsondiffpatchf: 'jsondiffpatch/src/main-formatters.js',
            gojs: 'gojs/release/go.js',
            vectorGrid: 'leaflet.vectorgrid/dist/Leaflet.VectorGrid.bundled.js',
        }
    },
    resolveLoader: {
        root: __dirname + '/node_modules'
    }
};
